﻿using System;
using System.Configuration;
using System.Web.Http;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Web.Cache;
using Umbraco.Web.WebApi;

namespace Our.Umbraco.Republish
{
    public class SimpleRepublishController : UmbracoApiController
    {
        public static string ApiKey = ConfigurationManager.AppSettings[nameof(SimpleRepublishController) + ":" + nameof(ApiKey)];

        [HttpGet]
        public bool RepublishAllNodes(string key = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(ApiKey) && key != ApiKey)
                {
                    ApplicationContext.ProfilingLogger.Logger.Warn<SimpleRepublishController>("Invalid API key in republish all call");
                    return false;
                }

                DistributedCache.Instance.RefreshAll(DistributedCache.PageCacheRefresherGuid);
                ApplicationContext.Services.ContentService.RePublishAll();
                return true;
            }
            catch (Exception ex)
            {
                ApplicationContext.ProfilingLogger.Logger.Error<SimpleRepublishController>("Could not trigger republish all", ex);
                return false;
            }
        }
    }
}
